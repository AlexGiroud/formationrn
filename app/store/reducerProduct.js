import {REST_ENV} from '../config/config';
import store from './store';

export const productInitialState = {products: []};
export const PRODUCTS_ACTION_TYPE = Object.seal({
  ADD_PRODUCT: 'ADD_PRODUCT',
  ADD_PRODUCTS: 'ADD_PRODUCTS',
  FETCH_PRODUCTS: 'FETCH_PRODUCTS',
});

export default function reducer(state = productInitialState, action) {
  switch (action.type) {
    case PRODUCTS_ACTION_TYPE.FETCH_PRODUCTS:
      fetch(`${REST_ENV}products`)
        .then(
          response => response.json(),
          () => {},
        )
        .then(response => {
          store.dispatch({
            type: PRODUCTS_ACTION_TYPE.ADD_PRODUCTS,
            values: response,
          });
        });
      return state;
    case PRODUCTS_ACTION_TYPE.ADD_PRODUCT:
      return {...state, products: [...state.products, action.value]};
    case PRODUCTS_ACTION_TYPE.ADD_PRODUCTS:
      return {...state, products: [...state.products, ...action.values]};
    default:
      return state;
  }
}
