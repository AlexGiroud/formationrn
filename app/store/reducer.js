import store from './store';
import {REST_ENV} from '../config/config';
import Main from '../components/Main/Main';
import React, {Component} from 'react';

export const initialState = {
  window: null,
  isGranted: false,
  login: 'gigi',
  password: 'gigi',
};
export const CORE_ACTION_TYPE = Object.seal({
  SET_PASSWORD: 'SET_PASSWORD',
  SET_LOGIN: 'SET_LOGIN',
  MAKE_AUTHENT: 'MAKE_AUTHENT',
  CHANGE_WINDOW: 'CHANGE_WINDOW',
});
const PRIVATE_CORE_ACTION_TYPE = Object.freeze({
  AUTH_SUCCESS: 'AUTH_SUCCESS',
  AUTH_FAIL: 'AUTH_FAIL',
});

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case PRIVATE_CORE_ACTION_TYPE.AUTH_SUCCESS:
      return {...state, isGranted: true};
    case PRIVATE_CORE_ACTION_TYPE.AUTH_FAIL:
      return {...state, login: '', password: ''};
    case CORE_ACTION_TYPE.CHANGE_WINDOW:
      return {...state, window: action.value};
    case CORE_ACTION_TYPE.SET_PASSWORD:
      return {...state, password: action.value};
    case CORE_ACTION_TYPE.SET_LOGIN:
      return {...state, login: action.value};
    case CORE_ACTION_TYPE.MAKE_AUTHENT:
      fetch(`${REST_ENV}users/?login=${state.login}&password=${state.password}`)
        .then(response => response.json())
        .then(response => {
          if (
            Array.isArray(response) &&
            response.length > 0 &&
            response[0].id
          ) {
            store.dispatch({type: PRIVATE_CORE_ACTION_TYPE.AUTH_SUCCESS});
          } else {
            store.dispatch({type: PRIVATE_CORE_ACTION_TYPE.AUTH_FAIL});
          }
        })
        .catch(function (error) {
          console.log(
            'There has been a problem with your fetch operation: ' +
              error.message,
          );
          // ADD THIS THROW error
          throw error;
        });
      return state;
    default:
      return state;
  }
}
