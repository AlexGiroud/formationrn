import React, {Component} from 'react';
import {createStore, combineReducers} from 'redux';
import reducer, {CORE_ACTION_TYPE} from './reducer';
import reducerProduct, {PRODUCTS_ACTION_TYPE} from './reducerProduct';
import reducerReceipe, {RECEIPE_ACTION_TYPE} from './reducerReceipe';

const combinedReducer = combineReducers({core: reducer, stock: reducerProduct, kitchen: reducerReceipe});
const store = createStore(combinedReducer);
store.subscribe(() => {
//   console.log(store.getState());
});

export default store;
