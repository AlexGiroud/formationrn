import {meiliClient} from '../config/config';
import store from './store';

export const receipeInitialState = {search: 'salade', receipes: []};
export const RECEIPE_ACTION_TYPE = Object.seal({
  FETCH_RECEIPES: 'FETCH_RECEIPES',
});

const PRIVATE_RECEIPE_TYPE = Object.seal({
  FETCH_FINISHED: 'FETCH_FINICHED',
});
const index = meiliClient.index('receipes');

export default function reducer(state = receipeInitialState, action) {
  switch (action.type) {
    case RECEIPE_ACTION_TYPE.FETCH_RECEIPES:
      index
        .search(action.value)
        .then(search => {
          store.dispatch({
            type: PRIVATE_RECEIPE_TYPE.FETCH_FINISHED,
            values: search.hits,
          });
        })
        .catch(function (error) {
          console.log(
            'There has been a problem with your fetch RECEIPES operation: ' +
              error.message,
          );
          throw error;
        });
      return {...state, search: action.value};
    case PRIVATE_RECEIPE_TYPE.FETCH_FINISHED:
      return {...state, receipes: action.values};
    default:
      return state;
  }
}
