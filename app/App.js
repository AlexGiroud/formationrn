import React, {Component} from 'react';
import Button from './components/Button/Button';
import {Text, SafeAreaView} from 'react-native';
import SplashImage from './components/SplashImage/SplashImage';
import Auth from './components/Auth/Auth';
import Main from './components/Main/Main';
import store from './store/store';
import {CORE_ACTION_TYPE} from './store/reducer';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      window: <SplashImage onEndSplash={() => this.onSplashEnd()} />,
      isGranted: false,
      login: undefined,
    };
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState(store.getState().core);
    });
    store.dispatch({
      type: CORE_ACTION_TYPE.CHANGE_WINDOW,
      value: <SplashImage onEndSplash={() => this.onSplashEnd()} />,
    });
  }

  componentDidUpdate() {}

  onSplashEnd() {
    store.dispatch({
      type: CORE_ACTION_TYPE.CHANGE_WINDOW,
      value: <Auth onConnect={() => this.onSuccessConnect()} />,
    });
  }

  onSuccessConnect(login) {
    store.dispatch({
      type: CORE_ACTION_TYPE.CHANGE_WINDOW,
      value: <Main />,
    });
  }

  render() {
    return <SafeAreaView style={{flex: 1}}>{this.state.window}</SafeAreaView>;
  }
}
