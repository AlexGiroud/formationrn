import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  Text,
  StyleSheet,
  View,
  FlatList,
  TextInput,
  Button,
} from 'react-native';
import Receipe from '../Receipe/Receipe';
import store from '../../store/store';
import {RECEIPE_ACTION_TYPE} from '../../store/reducerReceipe';
import List from '../List/List';
import { PRODUCTS_ACTION_TYPE } from '../../store/reducerProduct';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {search: 'salade', receipes: []};
    store.dispatch({type: RECEIPE_ACTION_TYPE.FETCH_RECEIPES});
    store.dispatch({type: PRODUCTS_ACTION_TYPE.FETCH_PRODUCTS});
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState(store.getState().kitchen);
    });
  }

  render() {
    return (
      <View data-testid="Main" style={styles.topContainer}>
        {/* <View style={styles.container}>
          <TextInput
            style={styles.input}
            onChangeText={value => {
              store.dispatch({
                type: RECEIPE_ACTION_TYPE.FETCH_RECEIPES,
                value: value,
              });
            }}
            value={this.state.search}
            placeholder="Recherche"
            placeholderTextColor="#000"
            textColor="#000"
            backgroundColor="#fff"
          />
        </View>
        <FlatList
          contentContainerStyle={styles.liste}
          data={this.state.receipes}
          renderItem={({item}) => <Receipe hit={item} />}
        /> */}
        <List />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  topContainer: {
    backgroundColor: '#80ADD9',
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  input: {
    width: '100%',
    color: '#000',
  },
  button: {
    height: '100%',
  },
  liste: {},
});

Main.propTypes = {};

Main.defaultProps = {};

export default Main;
