import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import SplashImage from './SplashImage';

describe('<SplashImage />', () => {
  test('it should mount', () => {
    render(<SplashImage />);
    
    const splashImage = screen.getByTestId('SplashImage');

    expect(splashImage).toBeInTheDocument();
  });
});