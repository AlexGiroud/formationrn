import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {View, Text, Image, StyleSheet} from 'react-native';

export default function SplashImage(props) {
  const [second, setsecond] = useState({count: 2});

  useEffect(() => {
    reduceSecond();
  });

  useEffect(() => {
    console.log('Init Splash screen');
  }, []);

  const reduceSecond = () => {
    setTimeout(() => {
      if (second.count > 0) {
        setsecond({count: second.count - 1});
      } else {
        props.onEndSplash();
      }
    }, 1000);
  };

  return (
    <View data-testid="SplashImage" style={styles.container}>
      <Image
        style={styles.logo}
        source={{
          uri: 'https://media.giphy.com/media/Cmr1OMJ2FN0B2/giphy.gif',
        }}
      />
      <Text style={styles.title}>App formation</Text>
      <Text style={styles.countdown}>Patientez {second.count}s</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  logo: {
    alignSelf: 'center',
    width: 300,
    height: 400,
  },
  title: {
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 40,
    color: '#80ADD9',
  },
  countdown: {
    textAlign: 'center',
    color: '#6AD996',
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#D9AB5F',
    flex: 1,
    justifyContent: 'center',
  },
});

SplashImage.propTypes = {
  onEndSplash: PropTypes.func.isRequired,
};

SplashImage.defaultProps = {};
