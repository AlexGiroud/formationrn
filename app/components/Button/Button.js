import React from 'react';
import PropTypes from 'prop-types';
import {Text, StyleSheet, TouchableHighlight, View} from 'react-native';

export default function Button(props) {
  return (
    <View>
      <TouchableHighlight
        onPress={props.onclick}
        data-testID="Button"
        style={{...style.container, ...props.Color}}>
        <Text style={style.text}>{props.Text} 🍺</Text>
      </TouchableHighlight>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    backgroundColor: 'grey',
    alignSelf: 'center',
    alignContent: 'center',
    padding: 5,
    margin: 5,
    width: '30%',
  },
  text: {
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold'
  },
  blue: {
    backgroundColor: 'blue',
  },
});

Button.propTypes = {
  Text: PropTypes.string.isRequired,
  Color: PropTypes.object,
  onclick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  Color: style.blue,
};
