import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ListElement from './ListElement';

describe('<ListElement />', () => {
  test('it should mount', () => {
    render(<ListElement />);
    
    const listElement = screen.getByTestId('ListElement');

    expect(listElement).toBeInTheDocument();
  });
});