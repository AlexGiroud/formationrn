import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Button,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';

export default function ListElement(props) {
  return (
    <TouchableHighlight
      onPress={() => {props.pressItem(props.product.id)}}
      style={styles.container}>
      <>
      <Text style={styles.title}>{props.product.name}</Text>
      {props.product.selected && (
        <Image
          style={styles.image}
          source={{
            uri: props.product.img,
          }}
        />
      )}
      {props.product.selected && (
        <View style={styles.subtitle_block}>
          <Text numberOfLines={7}>{props.product.description}</Text>
          <Text style={styles.price}>{props.product.prix}€</Text>
          <View style={styles.button}>
            <Button
              style={styles.button}
              onPress={() => {
                props.buttonPress(props.product);
              }}
              title="Ajouter🛒"
            />
          </View>
        </View>
      )}
      </>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    padding: 5,
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#75DAD6',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowColor: 'black',
    shadowOffset: {height: 8, width: 3},
  },
  title: {
    color: 'black',
    fontWeight: 'bold',
    width: '100%',
    fontSize: 20,
    marginBottom: 5,
  },
  subtitle_block: {
    width: '70%',
  },
  image: {
    width: '25%',
    height: '70%',
    alignSelf: 'stretch',
    marginBottom: 5
  },
  price: {
    fontWeight: 'bold',
    alignSelf: 'flex-end',
    marginTop: 'auto',
    marginBottom: 5
  },
  button: {
    alignSelf: 'flex-end',
    marginTop: 'auto',
    backgroundColor: '#D9AB5F',
    color: '#000000',
    marginBottom: 5
  },
});

ListElement.propTypes = {};

ListElement.defaultProps = {};
