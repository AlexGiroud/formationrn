import React from 'react';
import PropTypes from 'prop-types';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import Button from '../Button/Button';
import store from '../../store/store';
import {CORE_ACTION_TYPE} from '../../store/reducer';

export default function Auth(props) {
  const [loginDatas, setloginDatas] = React.useState({
    login: 'gigi',
    password: 'gigi',
  });

  React.useEffect(() => {
    setloginDatas({
      login: store.getState().core.login,
      password: store.getState().core.password,
    });
    let unsubscribe = store.subscribe(() => {
      setloginDatas({
        login: store.getState().core.login,
        password: store.getState().core.password,
      });
    });
    return function cleanup() {
      unsubscribe();
    };
  }, []);
  React.useEffect(() => {
    if (store.getState().core.isGranted) props.onConnect();
  });

  let loginOnline = function () {
    store.dispatch({type: CORE_ACTION_TYPE.MAKE_AUTHENT});
  };

  return (
    <View data-testid="Auth" style={styles.container}>
      <View style={styles.subContainer}>
        <Text style={styles.title}>Authentification</Text>
        <TextInput
          style={styles.input}
          onChangeText={value => {
            store.dispatch({type: CORE_ACTION_TYPE.SET_LOGIN, value: value});
          }}
          value={loginDatas.login}
          placeholder="Login"
          placeholderTextColor="#000"
        />
        <TextInput
          style={styles.input}
          onChangeText={value => {
            store.dispatch({type: CORE_ACTION_TYPE.SET_LOGIN, value: value});
          }}
          value={loginDatas.password}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="#000"
        />
        <Button
          Text="Se connecter"
          onclick={() => loginOnline()}
          Color={styles.button}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  subContainer: {
    width: '80%',
    backgroundColor: '#75DAD6',
    padding: 5,
    shadowOpacity: 0.5,
    shadowRadius: 8,
    shadowColor: 'black',
    shadowOffset: {height: 8, width: 3},
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    marginBottom: 10,
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#75DAD6',
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    backgroundColor: 'white',
    color: 'black',
    width: '100%',
    height: 40,
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#D98F8B',
    color: 'black',
    width: '100%',
    height: 40,
    justifyContent: 'center',
  },
});

Auth.propTypes = {
  onConnect: PropTypes.func.isRequired,
};

Auth.defaultProps = {};
