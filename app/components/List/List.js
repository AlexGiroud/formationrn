import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  StyleSheet,
  Dimensions,
  FlatList,
  ToastAndroid,
} from 'react-native';
import PropTypes from 'prop-types';
import store from '../../store/store';
import ListElement from '../ListElement/ListElement';

export default function List() {
  const [products, setProducts] = React.useState([]);
  const [basket, addToBasket] = React.useState([]);
  const [search, setFindValue] = React.useState('');

  React.useEffect(() => {
    let unsubscribe = store.subscribe(() => {
      setProducts(store.getState().stock.products);
    });
    return function cleanup() {
      unsubscribe();
    };
  }, []);

  function clickBasket(product) {
    ToastAndroid.show(`Vous venez d'ajouter ${product.name} au 🛒 !`, ToastAndroid.SHORT);
    addToBasket([...basket, product]);
    console.log(basket);
  }

  function pressItem(id) {
    products
      .filter(pro => {
        return pro.id === id;
      })
      .forEach(product => {
        product.selected = !product.selected;
      });
    setProducts([...products]);
  }

  return (
    <View>
      <Text style={styles.title}>Liste des Produits</Text>
      <TextInput
        style={{height: 40}}
        onChangeText={value => setFindValue(value)}
        placeholder="Recherche"
        placeholderTextColor="#000"
        textColor="#000"
        backgroundColor="#fff"
      />
      <View style={StyleSheet.liste}>
        <ScrollView>
          {products
            .filter(o => {
              return o.name.toLowerCase().includes(search.toLowerCase());
            })
            .map((e, i) => {
              return (
                <ListElement
                  product={e}
                  key={'p-' + i}
                  buttonPress={clickBasket}
                  pressItem={pressItem}
                />
              );
            })}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  liste: {
    height: Dimensions.get('window').height - 60,
    width: '100%',
    backgroundColor: '#6AD996',
  },
  title: {
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    width: '100%',
    fontSize: 20,
    marginBottom: 5,
  },
  flatliste: {},
});

List.propTypes = {};

List.defaultProps = {};
