import React from 'react';
import {View, Text, Image, StyleSheet, TouchableHighlight, Linking} from 'react-native';
import PropTypes from 'prop-types';

export default function Receipe(props) {
  return (
    <TouchableHighlight
      onPress={() => {
        Linking.openURL(props.hit.url);
      }}>
      <View data-testid="Receipe" style={styles.container}>
        <Text style={styles.title}>{props.hit.title}</Text>
        <Image
          style={styles.image}
          source={{
            uri: props.hit.imgUrl,
          }}
        />
        <Text style={styles.subtitle} numberOfLines={7}>
          {props.hit.description}
        </Text>
      </View>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    padding: 5,
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#75DAD6',
    minHeight: 60,
    alignItems: 'stretch',
    justifyContent: 'space-between',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowColor: 'black',
    shadowOffset: {height: 8, width: 3},
  },
  title: {
    color: 'black',
    fontWeight: 'bold',
    width: '100%',
    fontSize: 20,
    marginBottom: 5,
  },
  subtitle: {
    width: '70%',
  },
  image: {
    width: '25%',
    height: '50%',
    alignSelf: 'stretch',
  },
});

Receipe.propTypes = {
  hit: PropTypes.object.isRequired,
};

Receipe.defaultProps = {};
