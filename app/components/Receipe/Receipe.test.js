import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Receipe from './Receipe';

describe('<Receipe />', () => {
  test('it should mount', () => {
    render(<Receipe />);
    
    const receipe = screen.getByTestId('Receipe');

    expect(receipe).toBeInTheDocument();
  });
});